import os, sys
sys.path.append(os.path.abspath('.'))
from Core.training import train
from Core.LineDetect.classes import classes
if __name__ == "__main__":
  path_save_model = 'Models/LineDetect/line1.h5'
  name_train_folder = 'Data/LineDetect/Train'
  classes = len(classes)
  epochs=20
  train(
    path_save_model=path_save_model,
    name_train_folder=name_train_folder,
    classes=classes,
    epochs=epochs
  )