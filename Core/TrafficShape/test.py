import os, sys
sys.path.append(os.path.abspath('.'))
from Core.testing import Testing
from Core.TrafficShape.classes import classes,model

if __name__ == "__main__":
  main = Testing(classes=classes,model=model)
  main.test()