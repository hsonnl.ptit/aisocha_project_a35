import socketio
from helper.imgProsessing import numpyImg2Bit64
import cv2
import numpy as np
from config.variable import url_camera

EVENT_CLIENT_RECEIVE_FRAME = 'received_frame'
EVENT_CLIENT_CHECK_IN_RESULT = 'receive_data'




def get_video_capture():
    return cv2.VideoCapture(url_camera)


def show_camera_by_frame():
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()

def get_camera(url=url_camera):
    global streamming
    global checking
    global count
    global is_checking
    is_checking = True
    count= 0
    cap = get_video_capture()
    while(True):
        _, frame = cap.read()
        try:
            cv2.imshow("demo",frame)
            if is_checking:
                my_message(frame)
        except:
            pass
        show_camera_by_frame();
    cap.release()
    cv2.destroyAllWindows()

sio = socketio.Client()

sio.event
def connect():
    print('connection established')

@sio.on(EVENT_CLIENT_CHECK_IN_RESULT)
def handle_event_client_receive_frame(data):
    global is_checking
    is_checking = True
    if data !='none' :
        if data != []:
            print(data,type(data));


def my_message(data):
    global is_checking
    is_checking = False
    sio.emit(EVENT_CLIENT_RECEIVE_FRAME, numpyImg2Bit64(data))

sio.event
def disconnect():
    print('disconnected from server')


sio.connect('http://127.0.0.1:5002/')
get_camera(url_camera)

