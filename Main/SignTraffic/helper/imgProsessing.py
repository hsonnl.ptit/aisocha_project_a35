import base64
import cv2
import numpy as np

def numpyImg2Bit64(numpyimg):
    return base64.b64encode(cv2.imencode('.jpg', numpyimg)[1]).decode()

def bit642NumpyImg(encode_string):
    jpg_original = base64.b64decode(encode_string)
    jpg_as_np = np.frombuffer(jpg_original, dtype=np.uint8)
    return cv2.imdecode(jpg_as_np, flags=1)