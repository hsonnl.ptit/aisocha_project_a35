import socketio
import cv2
import numpy as np
import json
import base64
from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO


from Recognition import detection ,detectPlateShape,detectPlateType
from detec import mainDetect
from helper.imgProsessing import bit642NumpyImg, numpyImg2Bit64
from config.variable import url_server

EVENT_SERVER_EMIT_CHECKED_RESULT = "trafficSign"
EVENT_CLIENT_RECEIVE_FRAME = 'received_frame'
EVENT_CLIENT_CHECK_IN_RESULT = 'receive_data'

# net = cv2.dnn.readNet()

def socket_io_init():
    global app
    global socket_server
    global socket_client
    socket_client = socketio.Client()
    socket_client.connect(url_server)
    socket_server = SocketIO(app, logger=True, cors_allowed_origins="*")

    @socket_server.on(EVENT_CLIENT_RECEIVE_FRAME)
    def handle_event_client_receive_frame(data):
        img = bit642NumpyImg(data)
        img = mainDetect(img)
        res_arr = []
        try:
            count = 0
            for im in img:
                
                if type(im) == type(None):
                    count+=1
                else:
                    height, width = im.shape[:2]
                    if(0.85<height/width and height/width<1.25):
                        res = detectPlateType(im)
                        print(res)
                        if res!="Unknown":
                            res_arr.append(res)
                            emit_result_checked_in_to_server(res)
                        print(count,width,height,res)
            print(type(res_arr))
            emit_result_checked_in_to_server(json.dumps(res_arr))
            emit_client_check_in_result(res_arr)
        except:
            print("im none")
            count = 0
            emit_client_check_in_result(res_arr)
        
def emit_client_check_in_result(data):
    global socket_server
    socket_server.emit(EVENT_CLIENT_CHECK_IN_RESULT,data)

def emit_result_checked_in_to_server(data):
    global socket_client
    socket_client.emit(EVENT_SERVER_EMIT_CHECKED_RESULT,{"sign":data})

def main():
    global app
    app = Flask(__name__)
    CORS(app, resources=r'/*', origins="*", supports_credentials=True)
    socket_io_init()
    app.run(port=5002)


if __name__ == "__main__":
    main()
