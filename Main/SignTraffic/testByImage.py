import cv2
from Recognition import detection ,detectPlateShape,detectPlateType
from detec import mainDetect

def main():
    img = cv2.imread("DetectionSignTraffic/img/imgTemp.png")
    res_arr = []
    try:
      img = mainDetect(img)
      for im in img:
        cv2.imshow("crop",im)
        cv2.waitKey(1000)
        res = detection(im)
        res_arr.append(res)
    except:
      print("Error")
    print(res_arr)


if __name__ == "__main__":
    main()