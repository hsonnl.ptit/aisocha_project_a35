
import cv2
import numpy as np
import imutils
from Main.SignTraffic.Recognition import detection ,detectPlateShape,detectPlateType
import uuid
from Main.SignTraffic.config.variable import model_path, model_shape_path,url_save_folder

def remove_other_color(img):
    frame = cv2.GaussianBlur(img, (3,3), 0)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # define range of blue color in HSV
    lower_blue = np.array([100,128,0])
    upper_blue = np.array([215,255,255])
    # Threshold the HSV image to get only blue colors
    mask_blue = cv2.inRange(hsv, lower_blue, upper_blue)

    lower_white = np.array([0,0,128], dtype=np.uint8)
    upper_white = np.array([255,255,255], dtype=np.uint8)
    # Threshold the HSV image to get only blue colors
    mask_white = cv2.inRange(hsv, lower_white, upper_white)

    lower_black = np.array([0,0,0], dtype=np.uint8)
    upper_black = np.array([170,150,50], dtype=np.uint8)

    mask_black = cv2.inRange(hsv, lower_black, upper_black)

    mask_1 = cv2.bitwise_or(mask_blue, mask_white)
    mask = cv2.bitwise_or(mask_1, mask_black)
    return mask
def remove_line(img):
    gray = img.copy()
    edges = cv2.Canny(gray,50,150,apertureSize = 3)
    minLineLength = 5
    maxLineGap = 3
    lines = cv2.HoughLinesP(edges,1,np.pi/180,15,minLineLength,maxLineGap)
    mask = np.ones(img.shape[:2], dtype="uint8") * 255
    if lines is not None:
        for line in lines:
            for x1,y1,x2,y2 in line:
                cv2.line(mask,(x1,y1),(x2,y2),(0,0,0),2)
    return cv2.bitwise_and(img, img, mask=mask)

def constrastLimit(image):
    img_hist_equalized = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
    channels = cv2.split(img_hist_equalized)
    channels[0] = cv2.equalizeHist(channels[0])
    img_hist_equalized = cv2.merge(channels)
    img_hist_equalized = cv2.cvtColor(img_hist_equalized, cv2.COLOR_YCrCb2BGR)
    return img_hist_equalized

def LaplacianOfGaussian(image):
    image[:,:,1] = 0

    LoG_image = cv2.GaussianBlur(image, (3,3), 0)           # paramter 
    gray = cv2.cvtColor( LoG_image, cv2.COLOR_BGR2GRAY)
    LoG_image = cv2.Laplacian( gray, cv2.CV_8U,3,3,2)       # parameter
    LoG_image = cv2.convertScaleAbs(LoG_image)
    return LoG_image
    
def binarization(image):
    thresh = cv2.threshold(image,32,255,cv2.THRESH_BINARY)[1]
    #thresh = cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    return thresh

def preprocess_image(image):
    # image = constrastLimit(image)
    # cv2.imshow("22",image)
    image = LaplacianOfGaussian(image)
    # cv2.imshow("223",image)
    image = binarization(image)
    # cv2.imshow("224",image)
    # kernel = np.ones((5,5),np.uint8)
    # image = cv.dilate(img,kernel,iterations = 1)
    # cv2.imshow("225",image)
    return image

def removeSmallComponents(image, threshold):
    #find all your connected components (white blobs in your image)
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(image, connectivity=8)
    sizes = stats[1:, -1]; nb_components = nb_components - 1

    img2 = np.zeros((output.shape),dtype = np.uint8)
    #for every component in the image, you keep it only if it's above threshold
    for i in range(0, nb_components):
        if sizes[i] >= threshold:
            img2[output == i + 1] = 255
    return img2
def findContour(image):
    #find contours in the thresholded image
    cnts = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    # cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    return cnts[0]
    
def cropContour(image, center, max_distance):
    width = image.shape[1]
    height = image.shape[0]
    top = max([int(center[0] - max_distance), 0])
    bottom = min([int(center[0] + max_distance + 1), height-1])
    left = max([int(center[1] - max_distance), 0])
    right = min([int(center[1] + max_distance+1), width-1])
    # print(left, right, top, bottom)
    return image[left:right, top:bottom]

def cropSign(image, coordinate):
    width = image.shape[1]
    height = image.shape[0]
    top = max([int(coordinate[0][1]), 0])
    bottom = min([int(coordinate[1][1]), height-1])
    left = max([int(coordinate[0][0]), 0])
    right = min([int(coordinate[1][0]), width-1])
    #print(top,left,bottom,right)
    return image[top:bottom,left:right]
def contourIsSign(perimeter, centroid, threshold):
    #  perimeter, centroid, threshold
    # # Compute signature of contour
    result=[]
    for p in perimeter:
        p = p[0]
        distance = np.sqrt((p[0] - centroid[0])**2 + (p[1] - centroid[1])**2) #<== ratio of circle
        result.append(distance)
    max_value = max(result)
    signature = [float(dist) / max_value for dist in result ]
    # Check signature of contour.
    temp = sum((1 - s) for s in signature)
    temp = temp / len(signature)
    if temp < threshold: # is  the sign
        return True, max_value + 2
    else:                 # is not the sign
        return False, max_value + 2
def findLargestSign(image, contours, threshold, distance_theshold):
    max_distance = 0
    coordinate = None
    sign = None
    '''#demo'''
    # boxes = []
    # image_copy = image.copy()
    # for c in contours:
    #     (x, y, w, h) = cv2.boundingRect(c)
    #     boxes.append([x,y, x+w,y+h])
    #     cv2.rectangle(image_copy, (x,y), (x+w,y+h), (255, 0, 0), 2)
    # print(boxes)
    # boxes = np.asarray(boxes)

    # cv2.imshow("image",image_copy);
    # cv2.waitKey(0)
    '''Read below'''
    sign_arr = []
    coordinate_arr = []
    for c in contours:
        M = cv2.moments(c)
        if M["m00"] == 0:
            continue
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        is_sign, distance = contourIsSign(c, [cX, cY], 1-threshold)
        if is_sign and distance > max_distance and distance > distance_theshold:
            max_distance = distance
            coordinate = np.reshape(c, [-1,2])
            left, top = np.amin(coordinate, axis=0)
            right, bottom = np.amax(coordinate, axis = 0)
            coordinate = [(left-20,top-20),(right+20,bottom+20)]
            sign = cropSign(image,coordinate)

            sign_arr.append(sign)
            coordinate_arr.append(coordinate)
    return sign_arr,coordinate_arr
def findLargestSignUsingModel(image, contours, threshold, distance_theshold):
    max_distance = 0
    coordinate = None
    sign = None
    '''Read below'''
    sign_arr = []
    coordinate_arr = []
    for c in contours:
        (x, y, w, h) = cv2.boundingRect(c)
        if w/h>1.25 or w/h<0.75:
            continue
        coordinate = np.reshape(c, [-1,2])
        left, top = np.amin(coordinate, axis=0)
        right, bottom = np.amax(coordinate, axis = 0)
        coordinate = [(left-2,top-2),(right+2,bottom+2)]
        
        sign = cropSign(image,coordinate)
        isPlate = detectPlateShape(sign)
        if isPlate == "none":
            continue;
        ''''''
        M = cv2.moments(c)
        if M["m00"] == 0:
            continue
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        is_sign, distance = contourIsSign(c, [cX, cY], 1-threshold)
        if is_sign==False and distance < distance_theshold:
            continue
        ''''''
        cv2.imwrite(url_save_folder+str(uuid.uuid4())+".jpg",sign)
        sign_arr.append(sign)
        coordinate_arr.append(coordinate)
    return sign_arr,coordinate_arr
###########
def mainDetect(img):
  imgo = img.copy()
  img = cv2.bitwise_and(img,img, mask=remove_other_color(imgo))
  img = preprocess_image(img)
#   img = removeSmallComponents(img,100)
  contours = findContour(img)
#   sign, coordinate = findLargestSign(imgo, contours, 0.65, 15)
  sign, coordinate = findLargestSignUsingModel(imgo, contours, 0.65,15)
  print(len(sign))
  return sign