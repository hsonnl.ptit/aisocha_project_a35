import os, sys
sys.path.append(os.path.abspath('.'))
from PIL import ImageTk, Image
import numpy
import cv2
from keras.models import load_model

from Main.SignTraffic.config.classesLable import classes, classes_shape, classes_shape_1
from Main.SignTraffic.config.variable import model_path, model_shape_path,url_save_folder
import uuid
model = load_model(model_path)
model_shape = load_model(model_shape_path)
def detection(image):
    # print("load done")
    #
    # cv2.imwrite(url_save_folder+str(uuid.uuid4())+".jpg",image)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(image)
    image = image.resize((30,30))
    image = numpy.expand_dims(image, axis=0)
    image = numpy.array(image)
    #
    classes_shape = classes_shape_1
    pred_shape = model_shape.predict_classes([image])[0]
    if pred_shape!=5:
        print("CLASSES SHAPE:",classes_shape[pred_shape+1])
        pred = model.predict_classes([image])[0]
        sign = classes[pred+1]
        return sign
    else:
        print("CLASSES SHAPE:",classes_shape[pred_shape+1])
        return classes_shape[pred_shape+1]

def detectPlateShape(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(image)
    image = image.resize((30,30))
    image = numpy.expand_dims(image, axis=0)
    image = numpy.array(image)
    #
    # classes_shape = {
    #     1:"circle-blue",
    #     2:"circle-outline",
    #     3:"circle-red",
    #     4:"circle-white",
    #     5:"triangle",
    #     6:"none",
    #     7:"none"
    # }
    classes_shape = {
        1:"none",
        2:"circle",
        3:"tri",
        # 4:"ssd"
    }

    pred_shape = model_shape.predict_classes([image])[0]
    return classes_shape[pred_shape+1]

def detectPlateType(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(image)
    image = image.resize((30,30))
    image = numpy.expand_dims(image, axis=0)
    image = numpy.array(image)
    #
    classes = { 
        1:'Speed limit (20km/h)',
        2:'Speed limit (30km/h)', 
        3:'Speed limit (50km/h)', 
        4:'Speed limit (60km/h)', 
        5:'Speed limit (70km/h)', 
        6:'Speed limit (80km/h)', 
        7:'End of speed limit (80km/h)', 
        8:'Speed limit (100km/h)', 
        9:'Speed limit (120km/h)', 
        10:'No passing', 
        11:'No passing veh over 3.5 tons', 
        12:'Right-of-way at intersection', 
        13:'Priority road', 
        14:'Yield', 
        15:'Stop', 
        16:'No vehicles', 
        17:'Veh > 3.5 tons prohibited', 
        18:'No entry', 
        19:'General caution', 
        20:'Dangerous curve left', 
        21:'Dangerous curve right', 
        22:'Double curve', 
        23:'Bumpy road', 
        24:'Slippery road', 
        25:'Road narrows on the right', 
        26:'Road work', 
        27:'Traffic signals', 
        28:'Pedestrians', 
        29:'Children crossing', 
        30:'Bicycles crossing', 
        31:'Beware of ice/snow',
        32:'Wild animals crossing', 
        33:'End speed + passing limits', 
        34:'Turn right ahead', 
        35:'Turn left ahead', 
        36:'Ahead only', 
        37:'Go straight or right', 
        38:'Go straight or left', 
        39:'Keep right', 
        40:'Keep left', 
        41:'Roundabout mandatory', 
        42:'End of no passing', 
        43:'End no passing veh > 3.5 tons',
        # 44:'none'
    }

    pred = model.predict_classes([image])[0]
    return classes[pred+1]
