import os, sys
sys.path.append(os.path.abspath('.'))
import cv2
import sys
import threading
import socketserver
import numpy as np

import socketio
from PIL import ImageTk, Image
import numpy
import cv2

import uuid

from Main.SignTraffic.Recognition import detectPlateType
from Main.SignTraffic.detec import mainDetect
import json



class SignTrafficDetectStreamHandler(socketserver.StreamRequestHandler):
    socketio_client = socketio.Client()
    socketio_client.connect('http://192.168.1.102:7050')
    stop_start = 0
    stop_finish = 0
    drive_time_after_stop = 0
    EVENT_SERVER_EMIT_CHECKED_RESULT = "trafficSign"
    speed = 1024
    arrow_right = 4
    arrow_left = 3
    arrow_down = 2
    arrow_up = 1
    
    def handle(self):
        stream_bytes = b' '
        stop_flag = False
        stop_sign_active = True
        def sendDataToServer(event_name,data):
            self.socketio_client.emit(event_name,data)
        def emit_result_checked_in_to_server(data):
            self.socketio_client.emit(self.EVENT_SERVER_EMIT_CHECKED_RESULT,{"sign":data})
        
        try:
            count = 0
            while True:
                stream_bytes += self.rfile.read(1024)
                first = stream_bytes.find(b'\xff\xd8')
                last = stream_bytes.find(b'\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    gray = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)
                    image = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    # lower half of the image
                    height, width = gray.shape
                    roi = gray[int(height/2):height, :]

                    cv2.imshow('image', image)
                    image_array = roi.reshape(1, int(height/2) * width).astype(np.float32)
                    '''
                    Detection
                    '''
                    res_arr = []
                    img = mainDetect(image)
                    count = 0
                    for im in img:
                        
                        if type(im) == type(None):
                            count+=1
                        else:
                            height, width = im.shape[:2]
                            if(0.85<height/width and height/width<1.25):
                                res = detectPlateType(im)
                                # print(res)
                                if res!="Unknown":
                                    res_arr.append(res)
                                    emit_result_checked_in_to_server(res)
                                # print(count,width,height,res)
                    if res_arr !=[]:
                        print(type(res_arr),res_arr)

                    emit_result_checked_in_to_server(json.dumps(res_arr))
                    '''
                    Detected
                    '''
                    self.stop_start = cv2.getTickCount()

                    if stop_sign_active is False:
                        self.drive_time_after_stop = (self.stop_start - self.stop_finish) / cv2.getTickFrequency()
                        if self.drive_time_after_stop > 5:
                            stop_sign_active = True

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        print("car stopped")
                        break
        except:
            print("error")
            count = 0
            emit_client_check_in_result([])
        finally:
            cv2.destroyAllWindows()
            sys.exit()