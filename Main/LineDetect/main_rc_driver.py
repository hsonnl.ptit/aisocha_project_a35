import os, sys
sys.path.append(os.path.abspath('.'))
import cv2
import sys
import threading
import socketserver
import numpy as np


# from model import NeuralNetwork
from Main.LineDetect.driver_helper import *
import socketio
from PIL import ImageTk, Image
import numpy
import cv2
from keras.models import load_model

# from config.classesLable import classes, classes_shape, classes_shape_1
# from config.variable import model_path, model_shape_path,url_save_folder
import uuid
from Main.SignTraffic.SignTrafficThread import SignTrafficDetectStreamHandler
model = load_model("Models/LineDetect/line1.h5")

# distance data measured by ultrasonic sensor
sensor_data = None



class SensorDataHandler(socketserver.BaseRequestHandler):
  
    data = " "

    def handle(self):
        global sensor_data
        while self.data:
            self.data = self.request.recv(1024)
            sensor_data = round(float(self.data), 1)
            # print "{} sent:".format(self.client_address[0])
            print(sensor_data)

class VideoStreamHandler(socketserver.StreamRequestHandler):
  
    # h1: stop sign, measured manually
    # h2: traffic light, measured manually
    h1 = 5.5  # cm
    h2 = 5.5

    obj_detection = ObjectDetection()
    socketio_client = socketio.Client()
    socketio_client.connect('http://192.168.1.102:7050')
    #rc_car = RCControl("/dev/tty.usbmodem1421") 

    # cascade classifiers
    stop_cascade = cv2.CascadeClassifier("Models/LineDetect/cascade_xml/stop_sign.xml")
    light_cascade = cv2.CascadeClassifier("Models/LineDetect/cascade_xml/traffic_light.xml")

    d_to_camera = DistanceToCamera()
    # hard coded thresholds for stopping, sensor 30cm, other two 25cm
    d_sensor_thresh = 30
    d_stop_light_thresh = 25
    d_stop_sign = d_stop_light_thresh
    d_light = d_stop_light_thresh

    stop_start = 0  # start time when stop at the stop sign
    stop_finish = 0
    stop_time = 0
    drive_time_after_stop = 0

    speed = 1024
    arrow_right = 4
    arrow_left = 3
    arrow_down = 2
    arrow_up = 1
    
    def handle(self):
        global sensor_data
        stream_bytes = b' '
        stop_flag = False
        stop_sign_active = True
        def detection(image):
          image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
          image = Image.fromarray(image)
          image = image.resize((30,30))
          image = numpy.expand_dims(image, axis=0)
          image = numpy.array(image)
          #
          classes = {
            1:"left",
            2:"right",
            3:"forward"
          }
          pred = model.predict_classes([image])[0]
          return pred
        def sendDataToServer(event_name,data):
            self.socketio_client.emit(event_name,data)

        try:
            count = 0
            # stream video frames one by one
            while True:
                # print("rc_stream video")
                stream_bytes += self.rfile.read(1024)
                first = stream_bytes.find(b'\xff\xd8')
                last = stream_bytes.find(b'\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    gray = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)
                    image = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)
                    # lower half of the image
                    height, width = gray.shape
                    roi = gray[int(height/2):height, :]

                    # object detection
                    v_param1 = self.obj_detection.detect(self.stop_cascade, gray, image)
                    v_param2 = self.obj_detection.detect(self.light_cascade, gray, image)

                    # distance measurement
                    if v_param1 > 0 or v_param2 > 0:
                        d1 = self.d_to_camera.calculate(v_param1, self.h1, 300, image)
                        d2 = self.d_to_camera.calculate(v_param2, self.h2, 100, image)
                        self.d_stop_sign = d1
                        self.d_light = d2

                    cv2.imshow('image', image)

                    # reshape image
                    image_array = roi.reshape(1, int(height/2) * width).astype(np.float32)

                    # neural network makes prediction

                    # prediction = self.nn.predict(image_array)
                    prediction = detection(image)
                    # print("PREDICTION: ",prediction)

                    # stop conditions
                    if sensor_data and int(sensor_data) < self.d_sensor_thresh:
                        print("Stop, obstacle in front")
                        #self.rc_car.stop()
                        sensor_data = None

                    elif 0 < self.d_stop_sign < self.d_stop_light_thresh and stop_sign_active:
                        print("Stop sign ahead")
                        #self.rc_car.stop()

                        # stop for 5 seconds
                        if stop_flag is False:
                            self.stop_start = cv2.getTickCount()
                            stop_flag = True
                        self.stop_finish = cv2.getTickCount()

                        self.stop_time = (self.stop_finish - self.stop_start) / cv2.getTickFrequency()
                        print("Stop time: %.2fs" % self.stop_time)

                        # 5 seconds later, continue driving
                        if self.stop_time > 5:
                            print("Waited for 5 seconds")
                            stop_flag = False
                            stop_sign_active = False

                    elif 0 < self.d_light < self.d_stop_light_thresh:
                        # print("Traffic light ahead")
                        if self.obj_detection.red_light:
                            print("Red light")
                            #self.rc_car.stop()
                        elif self.obj_detection.green_light:
                            print("Green light")
                            pass
                        elif self.obj_detection.yellow_light:
                            print("Yellow light flashing")
                            pass

                        self.d_light = self.d_stop_light_thresh
                        self.obj_detection.red_light = False
                        self.obj_detection.green_light = False
                        self.obj_detection.yellow_light = False

                    else:
                        count=count+1
                        if count%12==0:
                            print(". . .",count)
                            sendDataToServer(event_name="driver",data={
                                    "control":self.arrow_up,
                                    "speed": 0
                                })
                        if count%6==0:
                            if prediction == 2:
                                sendDataToServer(event_name="driver",data={
                                        "control":self.arrow_up,
                                        "speed": 500
                                    })
                                print("|||||Forward|||||")
                        if count%9==0:
                            if prediction == 0:
                                sendDataToServer(event_name="driver",data={
                                        "control":self.arrow_left,
                                        "speed": 1024
                                    })
                                print("<<<<<<<<<<<<<<<<<<<<<<Left")
                            if prediction == 1:
                                sendDataToServer(event_name="driver",data={
                                        "control":self.arrow_right,
                                        "speed": 1024
                                    })
                                print("Right>>>>>>>>>>>>>>>>>>>>>")
                            # print("stop")
                        self.stop_start = cv2.getTickCount()
                        self.d_stop_sign = self.d_stop_light_thresh

                        if stop_sign_active is False:
                            self.drive_time_after_stop = (self.stop_start - self.stop_finish) / cv2.getTickFrequency()
                            if self.drive_time_after_stop > 5:
                                stop_sign_active = True

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        print("car stopped")
                        #self.rc_car.stop()
                        break
        except:
            print("error")
        finally:
            cv2.destroyAllWindows()
            sys.exit()


class Server(object):
    def __init__(self, host, port1, port2, port3):
        self.host = host
        self.port1 = port1
        self.port2 = port2
        self.port3 = port3

    def video_stream(self, host, port):
        s = socketserver.TCPServer((host, port), VideoStreamHandler)
        print("starting . . . video stream")
        s.serve_forever()

    def sensor_stream(self, host, port):
        s = socketserver.TCPServer((host, port), SensorDataHandler)
        print("starting . . . serve stream")
        s.serve_forever()
    
    def sign_traffic(self,host,port):
        s = socketserver.TCPServer((host,port),SignTrafficDetectStreamHandler)
        print("starting . . . sign traffic stream")
        s.serve_forever()

    def start(self):
        sensor_thread = threading.Thread(target=self.sensor_stream, args=(self.host, self.port2))
        sensor_thread.daemon = False
        sensor_thread.start()
        traffic_sign_thread = threading.Thread(target=self.sign_traffic, args=(self.host, self.port3))
        traffic_sign_thread.daemon = False
        traffic_sign_thread.start()
        self.video_stream(self.host, self.port1)
        
def mainLineDetect():
    h, p1, p2, p3 = "192.168.1.102", 8000, 8002, 8004

    ts = Server(h, p1, p2, p3)
    ts.start()

if __name__ == '__main__':
    mainLineDetect()
