__author__ = 'zhengwang'

import numpy as np
import cv2
import serial
import pygame
from pygame.locals import *
import socket
import time
import os
import json
import socketio
import uuid
class CollectTrainingData(object):
    
    def __init__(self, host, port, serial_port, input_size):

        self.server_socket = socket.socket()
        self.server_socket.bind((host, port))
        self.server_socket.listen(0)

        self.socketio_client = socketio.Client()
        self.socketio_client.connect('http://192.168.1.102:7050')
        self.speed = 1024

        # accept a single connection
        self.connection = self.server_socket.accept()[0].makefile('rb')

        # connect to a seral port
        #self.ser = serial.Serial(serial_port, 115200, timeout=1)
        self.send_inst = True

        self.input_size = input_size

        # create labels
        self.k = np.zeros((4, 4), 'float')
        for i in range(4):
            self.k[i, i] = 1

        pygame.init()
        pygame.display.set_mode((250, 250))

    def collect(self):
        def sendDataToServer(event_name,data):
            print(event_name,data)
            self.socketio_client.emit(event_name,data)
        arrow_right = 4
        arrow_left = 3
        arrow_down = 2
        arrow_up = 1
        saved_frame = 0
        total_frame = 0

        # collect images for training
        print("Start collecting images...")
        print("Press 'q' or 'x' to finish...")
        start = cv2.getTickCount()

        X = np.empty((0, self.input_size))
        y = np.empty((0, 4))

        # stream video frames one by one
        try:
            stream_bytes = b' '
            frame = 1
            while self.send_inst:
                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find(b'\xff\xd8')
                last = stream_bytes.find(b'\xff\xd9')

                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)
                    img = cv2.imdecode(np.frombuffer(jpg, dtype=np.uint8), cv2.IMREAD_COLOR)

                    
                    # select lower half of the image
                    height, width = image.shape
                    roi = image[int(height/2):height, :]

                    cv2.imshow('image', image)

                    # reshape the roi image into a vector
                    temp_array = roi.reshape(1, int(height/2) * width).astype(np.float32)
                    
                    frame += 1
                    total_frame += 1

                    # get input from human driver
                    for event in pygame.event.get():
                        if event.type == 2:
                            key_input = pygame.key.get_pressed()
                            if key_input[pygame.K_UP] and key_input[pygame.K_RIGHT]:
                                sendDataToServer(event_name="driver",data={
                                    "control":int(str(arrow_up)+str(arrow_right)),
                                    "speed": self.speed
                                })
                                print("Forward Right")

                                X = np.vstack((X, temp_array))
                                y = np.vstack((y, self.k[1]))
                                saved_frame += 1

                            elif key_input[pygame.K_UP] and key_input[pygame.K_LEFT]:
                                sendDataToServer(event_name="driver",data={
                                    "control":int(str(arrow_up)+str(arrow_left)),
                                    "speed": self.speed
                                })
                                print("Forward Left")
                                X = np.vstack((X, temp_array))
                                y = np.vstack((y, self.k[0]))
                                saved_frame += 1

                            elif key_input[pygame.K_DOWN] and key_input[pygame.K_RIGHT]:
                                sendDataToServer(event_name="driver",data={
                                    "control":int(str(arrow_down)+str(arrow_left)),
                                    "speed": self.speed
                                })
                                print("Reverse Right")

                            elif key_input[pygame.K_DOWN] and key_input[pygame.K_LEFT]:
                                sendDataToServer("driver",json.dumps({
                                    "control":int(str(arrow_down)+str(arrow_left)),
                                    "speed": self.speed
                                }))
                                print("Reverse Left")
                            elif key_input[pygame.K_UP]:
                                sendDataToServer(event_name="driver",data={
                                    "control":arrow_up,
                                    "speed": self.speed
                                })
                                saved_frame += 1
                                X = np.vstack((X, temp_array))
                                y = np.vstack((y, self.k[2]))
                                print("Forward",self.k[2])
                                cv2.imwrite("Data/LineDetect/Train/2/"+str(uuid.uuid4())+".jpg",img)

                            elif key_input[pygame.K_DOWN]:
                                sendDataToServer(event_name="driver",data={
                                    "control":arrow_down,
                                    "speed": self.speed
                                })
                                print("Reverse")

                            elif key_input[pygame.K_RIGHT]:
                                sendDataToServer(event_name="driver",data={
                                    "control":arrow_right,
                                    "speed": self.speed
                                })
                                X = np.vstack((X, temp_array))
                                y = np.vstack((y, self.k[1]))
                                saved_frame += 1
                                cv2.imwrite("Data/LineDetect/Train/1/"+str(uuid.uuid4())+".jpg",img)
                                print("Right",self.k[1])

                            elif key_input[pygame.K_LEFT]:
                                sendDataToServer(event_name="driver",data={
                                    "control":arrow_left,
                                    "speed": self.speed
                                })
                                X = np.vstack((X, temp_array))
                                y = np.vstack((y, self.k[0]))
                                saved_frame += 1
                                print("Left",self.k[0])
                                cv2.imwrite("Data/LineDetect/Train/0/"+str(uuid.uuid4())+".jpg",img)

                            elif key_input[pygame.K_x] or key_input[pygame.K_q]:

                                print("exit")
                                self.send_inst = False
                                break

                        elif event.type == pygame.KEYUP:
                            # self.ser.write(chr(0).encode())
                            sendDataToServer("driver",{
                                    "control":arrow_down,
                                    "speed": 0
                                })
                            print("down")

                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
            file_name = str(int(time.time()))
            directory = "computer/training_data"
            if not os.path.exists(directory):
                os.makedirs(directory)
            try:
                np.savez(directory + '/' + file_name + '.npz', train=X, train_labels=y)
            except IOError as e:
                print(e)

            end = cv2.getTickCount()

            print("Streaming duration: , %.2fs" % ((end - start) / cv2.getTickFrequency()))
            print(X.shape)
            print(y.shape)
            print("Total frame: ", total_frame)
            print("Saved frame: ", saved_frame)
            print("Dropped frame: ", total_frame - saved_frame)


        finally:
            self.connection.close()
            self.server_socket.close()
        print("end-game")


if __name__ == '__main__':
    # host, port
    h, p = "192.168.1.102", 8000

    # serial port
    sp = "/dev/tty.usbmodem1421"

    # vector size, half of the image
    s = 120 * 320

    ctd = CollectTrainingData(h, p, sp, s)
    ctd.collect()
