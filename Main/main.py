import os, sys
sys.path.append(os.path.abspath('.'))
import threading
import socketserver
from Main.SignTraffic.SignTrafficThread import SignTrafficDetectStreamHandler
from Main.LineDetect.main_rc_driver import SensorDataHandler,VideoStreamHandler
from Main.FaceMaskDetector.MaskDetectorThread import MaskDetectStreamHandler
class Server(object):
    def __init__(self, host, port1, port2, port3,port4):
        self.host = host
        self.port1 = port1
        self.port2 = port2
        self.port3 = port3
        self.port4 = port4

    def video_stream(self, host, port):
        s = socketserver.TCPServer((host, port), VideoStreamHandler)
        print("starting . . . video stream")
        s.serve_forever()

    def sensor_stream(self, host, port):
        s = socketserver.TCPServer((host, port), SensorDataHandler)
        print("starting . . . serve stream")
        s.serve_forever()
    
    def sign_traffic(self,host,port):
        s = socketserver.TCPServer((host,port), SignTrafficDetectStreamHandler)
        print("starting . . . sign traffic stream")
        s.serve_forever()

    def mask_detect(self,host,port):
        s = socketserver.TCPServer((host,port), MaskDetectStreamHandler)
        print("starting . . . mask stream")
        s.serve_forever()

    def start(self):
        # sensor_thread = threading.Thread(target=self.sensor_stream, args=(self.host, self.port2))
        # sensor_thread.daemon = False
        # sensor_thread.start()
        traffic_sign_thread = threading.Thread(target=self.sign_traffic, args=(self.host, self.port3))
        traffic_sign_thread.daemon = False
        traffic_sign_thread.start()
        # mask_thread = threading.Thread(target=self.mask_detect, args=(self.host, self.port4))
        # mask_thread.daemon = False
        # mask_thread.start()
        # self.video_stream(self.host, self.port1)
        
def mainLineDetect():
    h, p1, p2, p3,p4 = "192.168.1.102", 8000, 8002, 8004, 8008
    '''
    line:8000
    sensor:8002
    traffic sign:8004
    mask:8008
    '''
    ts = Server(h, p1, p2, p3, p4)
    ts.start()

if __name__ == '__main__':
    mainLineDetect()